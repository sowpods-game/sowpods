import sys

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
sensible_words = set()

with open('sowpods.txt', 'r') as file:
    sensible_words= set(word.strip() for word in file)


def replace_letter( word : str ) -> set:
    replacements = set()
    for i in range(len(word)):
        for letter in alphabet:
            if letter != word[i]:
                new_word = word[:i] + letter + word[i+1:]
                replacements.add(new_word)
    return replacements


def get_combinations( word : str ) -> set:
    def makes_sense(word):
        return word in sensible_words
    
    combinations = set()
    replacements = replace_letter(word)
    
    for new_word in replacements:
        if makes_sense(new_word):
            combinations.add(new_word)
    
    return combinations


def word_transformation( initial_word : str , final_word : str ) -> list[str]:
    
    def mark_visited(word : str , visited : set) -> set:
        return visited.add(word)

    path= initial_word
    
    if initial_word == final_word:
        return [path] 
    
    current_level = [(initial_word, [path])]
    visited = set([initial_word])
    
    while current_level:
        next_level = []
        
        for current_word, current_path in current_level:
            for combination in get_combinations(current_word):
                if combination == final_word:
                    return current_path + [combination]
                
                if combination not in visited:
                    mark_visited(combination,visited)
                    next_level.append((combination, current_path + [combination]))
        
        current_level = next_level
    
    return []


initial_word = 'ABOUT'
final_word  =  'HOUSE'
result = word_transformation( initial_word , final_word )

if result:
    print("Transformation sequence:", " -> ".join(result),"\nNumber of steps required: ",len(result))
else:
    print("No transformation possible")

